<?php
include 'includes/header.php';

if(isset($_POST['firstname_inp']) && isset($_POST['lastname_inp'])){
    $firstname = cleanInput($_POST['firstname_inp']);
    $lastname = cleanInput($_POST['lastname_inp']);

    if(ldapAddUser($ds, $firstname, $lastname)){
        header('Location: index.php');
    }
}
?>

<?php include 'includes/top.php'; ?>
<h3>Ajout d'utilisateur</h3>
<div class="row">
    <div class="col s4">
        <form action="addUser.php" method="POST">
            <input type="text" name="firstname_inp" placeholder="Prénom" required/>
            <input type="text" name="lastname_inp" placeholder="Nom de famille" required/>
            <button class="btn waves-effect waves-light" type="submit" name="validate_inp">Valider</button>
        </form>
    </div>
</div>
<?php include 'includes/footer.php'; ?>
