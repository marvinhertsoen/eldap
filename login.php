<?php
include 'includes/header.php';

if (isset($_SESSION['connected'])) {
    header('Location: index.php');
}
if (isset($_POST['login_inp']) && isset($_POST['password_inp'])) {
    $login = $_POST['login_inp'];
    $password = $_POST['password_inp'];

    $_SESSION["login"] = $login;
    $_SESSION["password"] = $password;
    header("Location: login.php");
}
?>

<?php include 'includes/top.php'; ?>
<div class="row">
    <form action="login.php" method="POST" class="col s4">
        <input type="text" name="login_inp" placeholder="Login" required/>
        <input type="password" name="password_inp" placeholder="Mot de passe" required/>
        <input type="submit" name="validate_inp" value="Valider"/>
    </form>
</div>

