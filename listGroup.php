<?php
require_once "includes/header.php";

ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
$entries = ldapGroupSearch($ds, '*');
?>

<?php include 'includes/top.php'; ?>
<h3>Gwoup list</h3>
<table id="entries" class="display" style="width:100%">
    <thead>
    <tr>
        <th>GroupID</th>
        <th>Group Name</th>
        <th>Group Description</th>
        <th>Users</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($entries as $entry): ?>
        <tr class="clickable-tr" href="editGroup.php?gid=<?= $entry['gidnumber'][0] ?>">
                <td><?= $entry['gidnumber'][0]?></td>
                <td><?= $entry['cn'][0]?></td>
                <td><?= isset($entry['description'][0]) ? $entry['description'][0] : "rien" ?></td>
                <td><?= getNbUsersInGroup($ds, $entry['gidnumber'][0])?></td>
                <td>
                    <a href="removeGroup.php?gid=<?= $entry['gidnumber'][0] ?>" class="waves-effect waves-light btn-small red"><i class="material-icons">delete</i></a>
                </td>
            </a>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<a href="addGroup.php" class="btn waves-effect waves-light">Add gwoupe</a>
    <script>
        $(document).ready(function() {
            $('#entries').DataTable();

            // clickable rows
            $('table tr.clickable-tr').click(function() {
                window.location.href = $(this).attr('href');
            });
        } );

    </script>
<?php

//echo 'Fermeture de la connexion';
require_once "includes/footer.php";
