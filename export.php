<?php
include 'includes/header.php';
if(!isAdmin()){
    header('Location: index.php');
}

if(isset($_POST['export_user'])){
    $ldap = json_encode(ldapUserSearch($ds, '*'));

    header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
    header("Content-Type: application/json");
    echo $ldap;
    exit();
}


?>

<?php include 'includes/top.php'; ?>
<h3>Export</h3>
<form action="export.php" method="POST">
    <form>
        <div class="form-group">
            <button class="btn waves-effect waves-light" type="submit" name="export_user">Export all users</button>
        </div>
    </form>
</form>


<?php include 'includes/footer.php'; ?>
