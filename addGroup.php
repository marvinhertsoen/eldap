<?php
include 'includes/header.php';

if(isset($_POST['nameInp'])){
    $groupName = cleanInput($_POST['nameInp']);
    if (ldapAddGroup($ds, $_POST['nameInp'])){
        header('Location: listGroup.php');
    }
}
?>

<?php include 'includes/top.php'; ?>
<h3>Ajout de groupe</h3>
<div class="row">
    <div class="col s4">
        <form action="addGroup.php" method="POST">
            <input type="text" name="nameInp" placeholder="Group name" required/>
            <button class="btn waves-effect waves-light" type="submit" name="validateInp">Valider</button>
        </form>
    </div>
</div>
<?php include 'includes/footer.php'; ?>
