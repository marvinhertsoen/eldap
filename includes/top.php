<html>
    <head>
        <title>Eldap</title>
        <link rel="stylesheet" href="resources/css/style.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    </head>
    <body>
    <div class="container">
        <nav>
            <div class="nav-wrapper">
                <a href="/" class="brand-logo">Eldap</a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <?php if (isConnected()): ?>
                        <li>
                            <a class="nav-link" href="index.php">Users</a>
                        </li>
                        <li>
                            <a class="nav-link" href="listGroup.php">Groups</a>
                        </li>
                        <?php if (isAdmin()): ?>
                        <li>
                            <a class="nav-link" href="export.php">Export</a>
                        </li>
                        <?php endif; ?>
                        <li>
                            <a class="nav-link" href="logout.php">Délogin</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </nav>
