<?php
include_once('./helpers/helper.php');
session_start();


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$login      = isset($_SESSION['login']) ? $_SESSION['login'] : null ;
$password   = isset($_SESSION['password']) ? $_SESSION['password'] : null;
$ds = connection($login, $password);


// redirect if not connected
if($_SERVER['REQUEST_URI'] !== '/login.php' && !isConnected()){
    header('Location: login.php');
}

 //@todo export / import
?>
