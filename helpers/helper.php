<?php

/**
 * Try to connect with username / password
 *
 * @param $login
 * @param $password
 * @return false|resource
 */
function connection($login, $password){
    $ds = ldap_connect('myldap');
    ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
    if ($ds) {
        if($login && $password){
            /*
             * dn: cn=admin,dc=bla,dc=com
             * dn: cn=Pemba Douceur,OU=PEOPLE,dc=bla,dc=com
             * //todo change bind_rdn bellow
             */

            // ADMIN
            $r = @ldap_bind($ds, 'cn='.$login.',dc=bla,dc=com', $password);
            if($r){
                $_SESSION['account_type'] = 'admin';
                $_SESSION['connected'] = true;
            }

            // PEOPLE
            if(!$r){
                $r = @ldap_bind($ds, 'cn='.$login.',ou=people,dc=bla,dc=com', $password);
                if($r){
                    $_SESSION['account_type'] = 'people';
                    $_SESSION['connected'] = true;
                }
            }
        }
    }
    else {
//        echo '<h4>Impossible de se connecter au serveur LDAP.</h4>';
    }
    return $ds;
}


/**
 * Check if user is undélogined
 *
 * @return bool
 */
function isConnected(){
    return isset($_SESSION['connected']);
}

/**
 * Check if user logged is an admin
 *
 * @return bool
 */
function isAdmin(){
    return isset($_SESSION['account_type']) && $_SESSION['account_type'] === 'admin';
}

/**
 * Return the last ldap huhuid
 *
 * @param $ds
 * @return int
 */
function getLastID($ds){
    $sr = ldap_search($ds, "dc=bla,dc=com", "gidNumber=*");
    $entries = ldap_get_entries($ds, $sr);
    unset($entries['count']);
    return count($entries);
}

/**
 * Search user from id
 *
 * @param $ds
 * @param $userId
 * @return array
 */
function ldapUserSearch($ds, $userId){
    $sr = @ldap_search($ds, "ou=people,dc=bla,dc=com", "uidNumber=" . $userId);
    return $sr  ? ldap_get_entries($ds, $sr) : [];
}

/**
 * Search group from id
 *
 * @param $ds
 * @param $groupId
 * @return array
 */
function ldapGroupSearch($ds, $groupId){
    $sr = @ldap_search($ds, "ou=group,dc=bla,dc=com", "gidNumber=" . $groupId);
    if($sr){
        $result = ldap_get_entries($ds, $sr);
        unset($result['count']);
        return $result;
    }
    return [];
}

/**
 * Add new group
 *
 * @param $ds
 * @param $groupName
 * @return bool
 */
function ldapAddGroup($ds, $groupName){
    $info['objectClass'] = ["top", "posixGroup"];
    $info['description'] = "description : " . $groupName;
    $info['gidNumber'] = getNextId($ds);
    return (ldap_add($ds, "cn=" . $groupName . ",ou=group,dc=bla,dc=com", $info));
}

/**
 * Edit a group
 *
 * @param $ds
 * @param $gid
 * @param $info
 * @param $dn
 * @return bool
 */
function ldapEditGroup($ds, $gid,  $info, $dn){
    return ldap_modify($ds, $dn, $info);
}

/**
 * Add a new user
 *
 * @param $ds
 * @param $firstname
 * @param $lastname
 * @return bool
 */
function ldapAddUser($ds, $firstname, $lastname){

    $info['objectClass'] = ["top", "person", "organizationalPerson", "inetOrgPerson", "posixAccount", "shadowAccount"];
    $info['sn'] = $lastname;
    $info['givenName'] = $firstname;
    $info['cn'] = $lastname. "_" . $firstname;
    $info['userPassword'] = 'bla';
    $info['homeDirectory'] = '/home/' . $firstname;
    $info['uidNumber'] = getNextId($ds);
    $info['gidNumber'] = getNextId($ds);
    $info['loginShell'] = '/bin/bash';
    $info['description'] = 'Utilisateur ' . $firstname . " " . $lastname;
    $info['uid'] = $firstname.'.'.$lastname;

    return ldap_add($ds, "cn=" . $info['cn'] . ",ou=people,dc=bla,dc=com", $info);
}

function getNbUsersInGroup($ds, $gidNumber){
    $group = ldapGroupSearch($ds, $gidNumber)[0];
    return isset($group['memberuid']) ? $group['memberuid']['count'] : 0;
}

/**
* Set to lower and remove htmlspecialchars and set ucfirst
*/
function cleanInput($string) {
  return ucfirst(strtolower(htmlspecialchars($string)));
}

/**
 * Get list of existing ids & add 1
 *
 * @param $ds
 * @return int|mixed
 */
function getNextId($ds){
    //todo replace by function
    $sr = ldap_search($ds, "dc=bla,dc=com", "gidNumber=*");
    $entries = ldap_get_entries($ds, $sr);
    unset($entries['count']);

    $ids = [];
    foreach ($entries as $entry){
        if(isset($entry['gidnumber'])){
            $ids[] = $entry['gidnumber'][0];
        }
        if(isset($entry['uidnumber'])){
            $ids[] = $entry['uidnumber'][0];
        }
    }

    return !empty($ids) ? max($ids) + 1 : 1100;
}

//@todo modif de groupe
//@todo export / import


function var_dump_pre($mixed = null) {
    echo '<pre>';
    var_dump($mixed);
    echo '</pre>';
    return null;
}
