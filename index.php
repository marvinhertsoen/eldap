<?php
require_once "includes/header.php";
ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
$entries = ldapUserSearch($ds, '*');
unset($entries['count']);
?>

<?php require_once "includes/top.php"; ?>
    <div>You have a <span style="color:#ee6e73;"><?= $_SESSION['account_type'] ?></span> account</div>

<!--<h3>Your groups</h3>-->
<!--<table id="entries" class="display" style="width:100%">-->
<!--    <thead>-->
<!--    <tr>-->
<!--        <th>GroupID</th>-->
<!--        <th>Group Name</th>-->
<!--        <th>Group Description</th>-->
<!--        <th>Users</th>-->
<!--        <th>Action</th>-->
<!--    </tr>-->
<!--    </thead>-->
<!--    <tbody>-->
<!--    --><?php //foreach($entries as $entry): ?>
<!--        <tr class="clickable-tr" href="editGroup.php?gid=--><?//= $entry['gidnumber'][0] ?><!--">-->
<!--            <td>--><?//= $entry['gidnumber'][0]?><!--</td>-->
<!--            <td>--><?//= $entry['cn'][0]?><!--</td>-->
<!--            <td>--><?//= isset($entry['description'][0]) ? $entry['description'][0] : "rien" ?><!--</td>-->
<!--            <td>--><?//= getNbUsersInGroup($ds, $entry['gidnumber'][0])?><!--</td>-->
<!--            <td style="text-align:center">-->
<!--                <a href="removeGroup.php?gid=--><?//= $entry['gidnumber'][0] ?><!--" class="waves-effect waves-light btn-small red"><i class="material-icons">delete</i></a>-->
<!--            </td>-->
<!--            </a>-->
<!--        </tr>-->
<!--    --><?php //endforeach; ?>
<!--    </tbody>-->
<!--</table>-->


<h3>User list</h3>
<table id="entries" class="display" style="width:100%">
    <thead>
    <tr>
        <th>UserId</th>
        <th>Firstname</th>
        <th>Name</th>
        <th>Description</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($entries as $entry): ?>
        <tr href="editUser.php?uid=<?= $entry['uidnumber'][0] ?>" class="clickable-tr">
            <td><?= $entry['uidnumber'][0] ?></td>
            <td><?= $entry['givenname'][0]?></td>
            <td><?= $entry['sn'][0]?></td>
            <td><?= $entry['description'][0]?></td>
            <td>
                <a href="removeUser.php?uid=<?= $entry['uidnumber'][0] ?>" class="waves-effect waves-light btn-small red"><i class="material-icons">delete</i></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<a href="addUser.php" class="btn waves-effect waves-light">Add user</a>

<script>
    $(document).ready(function() {
        $('#entries').DataTable();

        // clickable rows
        $('table tr.clickable-tr').click(function() {
            window.location.href = $(this).attr('href');
        });
    });
</script>



<?php
//echo 'Fermeture de la connexion';
require_once "includes/footer.php";
