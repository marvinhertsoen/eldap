<?php
include 'includes/header.php';

if(isset($_GET['gid'])){
    $group = ldapGroupSearch($ds, $_GET['gid']);
    $defaultDescription = isset($group[0]['description'][0]) ? $group[0]['description'][0] : '';
    $groupDescription   = isset($_GET['groupDescription']) ? $_GET['groupDescription'] : $defaultDescription;
    $groupDn = $group[0]['dn'];

    // Get current group users
    $groupUsers = isset($group[0]['memberuid']) ? $group[0]['memberuid'] : [] ;
    unset($groupUsers['count']);


    if(isset($_GET['groupDescription']) || isset($_GET['userGroupList'])){

        // Edit group description
        if(isset($_GET['groupDescription'])){
            ldapEditGroup($ds, $_GET['gid'], ['description'=> $groupDescription], $groupDn);
        }

        // Edit group users
        if(isset($_GET['userGroupList'])){
            ldapEditGroup($ds, $_GET['gid'], ['memberUid'=> $_GET['userGroupList']], $groupDn);
        } else{
            ldapEditGroup($ds, $_GET['gid'], ['memberUid'=> []], $groupDn);
        }
    }


    $userGroup = isset($_GET['userGroupList']) ? $_GET['userGroupList'] : $groupUsers;

    // Get list of all users
    $userList = ldapUserSearch($ds, '*');
    unset($userList['count']);
}
?>

<?php include 'includes/top.php'; ?>
<h3>Group : <span style="color:#ee6e73;"><?= $group[0]['cn'][0]?></span></h3>
<form action="editGroup.php" method="GET">
    <form>
        <div class="form-group">
            <label>Group description</label>
            <input type="text" class="form-control" name="groupDescription" placeholder="My group description" value="<?= $groupDescription ?>" required/>
        </div>
        <div class="form-group">
            <label>Users</label>
            <select multiple name="userGroupList[]">
                <?php foreach ($userList as $user): ?>
                    <option value="<?= $user['uid'][0]?>"
                        <?php echo in_array($user['uid'][0], $userGroup) ? 'selected' : "" ?>>
                    <?= $user['cn'][0] ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label>Number of users</label>
            <input type="text" class="form-control" value="<?= getNbUsersInGroup($ds, $_GET['gid'])?>" disabled/>
        </div>
        <input type="hidden" name="gid" value="<?= $_GET['gid'] ?>"/>
        <button class="btn waves-effect waves-light" type="submit" name="action">Validate</button>
    </form>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $('select').formSelect();
    });
</script>
<?php include 'includes/footer.php'; ?>
