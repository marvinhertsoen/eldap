# Eldap


Eldap is a ldap manager made with a lot of love and passion <3.
Here the features :
  - Group Management
  - User Management
  - Attaching Users to Groups
  - Users Export


### Tech

 Eldap uses a number of open source projects to work properly:

* [PHP] - Best programming language in the world <3
* [OpenLDAP] - Common implementation of the Lightweight Directory Access Protocol
* [Materialize] - As CSS Framework
* [jQuery] - The good old JS library
* [Docker] - The famous container software

And of course Eldap itself is open source with a [public repository][dill]
 on Gitlab.

### Installation

*First, shutdown your potential local apache2 & ldap servers.*

Eldap requires Docker and Docker-Compose to run.
Once you've installed them, simply launch the docker-compose file.
```sh
$ cd eldap
$ docker-compose up -d
```
Then you can access Eldap by navigating to the http://localhost/ address in your preferred browser.

### Default Accounts

At the first start, 2 default accounts will be created : 
* admin : bla (An administrator account)
* Toto_Toto : bla (A people account)

With an administrator account, you will be able to export all users into JSON. 

License
----

MIT


**Free Software, Hell Yeah!**

   [dill]: <https://gitlab.com/marvinhertsoen/eldap>
   [git-repo-url]: <https://gitlab.com/marvinhertsoen/eldap.git>
   [jQuery]: <http://jquery.com>
   [Materialize]: <https://materializecss.com>
   [Php]: <https://www.php.net>
   [OpenLDAP]: <https://www.openldap.org>
   [Docker]: <https://www.docker.com>
