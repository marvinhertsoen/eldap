<?php
include 'includes/header.php';

if(isset($_GET['uid'])){
    $entry = ldapUserSearch($ds, $_GET['uid']);

    $firstNameInp   = isset($_GET['firstNameInp']) ? $_GET['firstNameInp'] : $entry[0]['givenname'][0];
    $lastNameInp    = isset($_GET['lastNameInp']) ? $_GET['lastNameInp'] : $entry[0]['sn'][0];
    $descriptionInp = isset($_GET['descriptionInp']) ? $_GET['descriptionInp'] : $entry[0]['description'][0];
    $userDn = $entry[0]['dn'];

    if(isset($_GET['firstNameInp']) && isset($_GET['lastNameInp']) && isset($_GET['descriptionInp'])){
        $values["mail"] = "";
        ldap_modify($ds, $userDn,[
            'uid' => $entry[0]['uid'][0],
            'givenname' => $firstNameInp,
            'sn' => $lastNameInp,
            'description' => $descriptionInp
        ]);
        header('Location: index.php');
    }
}

?>


<?php include 'includes/top.php'; ?>
<h3>User : <span style="color:#ee6e73;"><?= $firstNameInp . ' ' . $lastNameInp?> </h3>
<form action="editUser.php" method="GET">
    <form>
        <div class="form-group">
            <label>Firstname</label>
            <input type="text" class="form-control" name="firstNameInp" placeholder="Jean" value="<?= $firstNameInp ?>" required/>
        </div>
        <div class="form-group">
            <label>Lastname</label>
            <input type="text" class="form-control" name="lastNameInp" placeholder="Dupond" value="<?= $lastNameInp ?>" required/>
        </div>
        <div class="form-group">
            <label>Description</label>
            <input type="text" class="form-control" name="descriptionInp" placeholder="Description" value="<?= $descriptionInp ?>" required/>
        </div>
        <input type="hidden" name="uid" value="<?= $_GET['uid'] ?>"/>
        <button class="btn waves-effect waves-light" type="submit" name="action">Validate</button>
    </form>
</form>



<script type="text/javascript">
    $(document).ready(function() {
        $('select').formSelect();
    });
</script>
<?php include 'includes/footer.php'; ?>
